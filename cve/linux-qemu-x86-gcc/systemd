LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2012-0871
CVE STATUS: Patched
CVE SUMMARY: The session_link_x11_socket function in login/logind-session.c in systemd-logind in systemd, possibly 37 and earlier, allows local users to create or overwrite arbitrary files via a symlink attack on the X11 user directory in /run/user/.
CVSS v2 BASE SCORE: 6.3
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2012-0871

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2012-1101
CVE STATUS: Patched
CVE SUMMARY: systemd 37-1 does not properly handle non-existent services, which causes a denial of service (failure of login procedure).
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 5.5
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2012-1101

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2012-1174
CVE STATUS: Patched
CVE SUMMARY: The rm_rf_children function in util.c in the systemd-logind login manager in systemd before 44, when logging out, allows local users to delete arbitrary files via a symlink attack on unspecified files, related to "particular records related with user session."
CVSS v2 BASE SCORE: 3.3
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2012-1174

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2013-4327
CVE STATUS: Patched
CVE SUMMARY: systemd does not properly use D-Bus for communication with a polkit authority, which allows local users to bypass intended access restrictions by leveraging a PolkitUnixProcess PolkitSubject race condition via a (1) setuid process or (2) pkexec process, a related issue to CVE-2013-4288.
CVSS v2 BASE SCORE: 6.9
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2013-4327

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2013-4391
CVE STATUS: Patched
CVE SUMMARY: Integer overflow in the valid_user_field function in journal/journald-native.c in systemd allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a large journal data field, which triggers a heap-based buffer overflow.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2013-4391

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2013-4392
CVE STATUS: Patched
CVE SUMMARY: systemd, when updating file permissions, allows local users to change the permissions and SELinux security contexts for arbitrary files via a symlink attack on unspecified files.
CVSS v2 BASE SCORE: 3.3
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2013-4392

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2013-4393
CVE STATUS: Patched
CVE SUMMARY: journald in systemd, when the origin of native messages is set to file, allows local users to cause a denial of service (logging service blocking) via a crafted file descriptor.
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2013-4393

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2013-4394
CVE STATUS: Patched
CVE SUMMARY: The SetX11Keyboard function in systemd, when PolicyKit Local Authority (PKLA) is used to change the group permissions on the X Keyboard Extension (XKB) layouts description, allows local users in the group to modify the Xorg X11 Server configuration file and possibly gain privileges via vectors involving "special and control characters."
CVSS v2 BASE SCORE: 5.9
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2013-4394

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2015-7510
CVE STATUS: Patched
CVE SUMMARY: Stack-based buffer overflow in the getpwnam and getgrnam functions of the NSS module nss-mymachines in systemd.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-7510

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2016-10156
CVE STATUS: Patched
CVE SUMMARY: A flaw in systemd v228 in /src/basic/fs-util.c caused world writable suid files to be created when using the systemd timers features, allowing local attackers to escalate their privileges to root. This is fixed in v229.
CVSS v2 BASE SCORE: 7.2
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-10156

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2016-7795
CVE STATUS: Patched
CVE SUMMARY: The manager_invoke_notify_message function in systemd 231 and earlier allows local users to cause a denial of service (assertion failure and PID 1 hang) via a zero-length message received over a notify socket.
CVSS v2 BASE SCORE: 4.9
CVSS v3 BASE SCORE: 5.5
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-7795

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2016-7796
CVE STATUS: Patched
CVE SUMMARY: The manager_dispatch_notify_fd function in systemd allows local users to cause a denial of service (system hang) via a zero-length message received over a notify socket, which causes an error to be returned and the notification handler to be disabled.
CVSS v2 BASE SCORE: 4.9
CVSS v3 BASE SCORE: 5.5
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-7796

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2017-1000082
CVE STATUS: Patched
CVE SUMMARY: systemd v233 and earlier fails to safely parse usernames starting with a numeric digit (e.g. "0day"), running the service in question with root privileges rather than the user intended.
CVSS v2 BASE SCORE: 10.0
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-1000082

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2017-15908
CVE STATUS: Patched
CVE SUMMARY: In systemd 223 through 235, a remote DNS server can respond with a custom crafted DNS NSEC resource record to trigger an infinite loop in the dns_packet_read_type_window() function of the 'systemd-resolved' service and cause a DoS of the affected service.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-15908

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2017-18078
CVE STATUS: Patched
CVE SUMMARY: systemd-tmpfiles in systemd before 237 attempts to support ownership/permission changes on hardlinked files even if the fs.protected_hardlinks sysctl is turned off, which allows local users to bypass intended access restrictions via vectors involving a hard link to a file for which the user lacks write access, as demonstrated by changing the ownership of the /etc/passwd file.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-18078

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2017-9217
CVE STATUS: Patched
CVE SUMMARY: systemd-resolved through 233 allows remote attackers to cause a denial of service (daemon crash) via a crafted DNS response with an empty question section.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-9217

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2017-9445
CVE STATUS: Patched
CVE SUMMARY: In systemd through 233, certain sizes passed to dns_packet_new in systemd-resolved can cause it to allocate a buffer that's too small. A malicious DNS server can exploit this via a response with a specially crafted TCP payload to trick systemd-resolved into allocating a buffer that's too small, and subsequently write arbitrary data beyond the end of it.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-9445

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2018-1049
CVE STATUS: Patched
CVE SUMMARY: In systemd prior to 234 a race condition exists between .mount and .automount units such that automount requests from kernel may not be serviced by systemd resulting in kernel holding the mountpoint and any processes that try to use said mount will hang. A race condition like this may lead to denial of service, until mount points are unmounted.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 5.9
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-1049

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2018-15686
CVE STATUS: Patched
CVE SUMMARY: A vulnerability in unit_deserialize of systemd allows an attacker to supply arbitrary state across systemd re-execution via NotifyAccess. This can be used to improperly influence systemd execution and possibly lead to root privilege escalation. Affected releases are systemd versions up to and including 239.
CVSS v2 BASE SCORE: 7.2
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-15686

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2018-15687
CVE STATUS: Patched
CVE SUMMARY: A race condition in chown_one() of systemd allows an attacker to cause systemd to set arbitrary permissions on arbitrary files. Affected releases are systemd versions up to and including 239.
CVSS v2 BASE SCORE: 6.9
CVSS v3 BASE SCORE: 7.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-15687

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2018-15688
CVE STATUS: Patched
CVE SUMMARY: A buffer overflow vulnerability in the dhcp6 client of systemd allows a malicious dhcp6 server to overwrite heap memory in systemd-networkd. Affected releases are systemd: versions up to and including 239.
CVSS v2 BASE SCORE: 5.8
CVSS v3 BASE SCORE: 8.8
VECTOR: ADJACENT_NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-15688

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2018-16864
CVE STATUS: Patched
CVE SUMMARY: An allocation of memory without limits, that could result in the stack clashing with another memory region, was discovered in systemd-journald when a program with long command line arguments calls syslog. A local attacker may use this flaw to crash systemd-journald or escalate his privileges. Versions through v240 are vulnerable.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-16864

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2018-16865
CVE STATUS: Patched
CVE SUMMARY: An allocation of memory without limits, that could result in the stack clashing with another memory region, was discovered in systemd-journald when many entries are sent to the journal socket. A local attacker, or a remote one if systemd-journal-remote is used, may use this flaw to crash systemd-journald or execute code with journald privileges. Versions through v240 are vulnerable.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-16865

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2018-16866
CVE STATUS: Patched
CVE SUMMARY: An out of bounds read was discovered in systemd-journald in the way it parses log messages that terminate with a colon ':'. A local attacker can use this flaw to disclose process memory data. Versions from v221 to v239 are vulnerable.
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 3.3
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-16866

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2018-16888
CVE STATUS: Patched
CVE SUMMARY: It was discovered systemd does not correctly check the content of PIDFile files before using it to kill processes. When a service is run from an unprivileged user (e.g. User field set in the service file), a local attacker who is able to write to the PIDFile of the mentioned service may use this flaw to trick systemd into killing other services and/or privileged processes. Versions before v237 are vulnerable.
CVSS v2 BASE SCORE: 1.9
CVSS v3 BASE SCORE: 4.7
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-16888

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2018-20839
CVE STATUS: Patched
CVE SUMMARY: systemd 242 changes the VT1 mode upon a logout, which allows attackers to read cleartext passwords in certain circumstances, such as watching a shutdown, or using Ctrl-Alt-F1 and Ctrl-Alt-F2. This occurs because the KDGKBMODE (aka current keyboard mode) check is mishandled.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-20839

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2018-21029
CVE STATUS: Patched
CVE SUMMARY: ** DISPUTED ** systemd 239 through 245 accepts any certificate signed by a trusted certificate authority for DNS Over TLS. Server Name Indication (SNI) is not sent, and there is no hostname validation with the GnuTLS backend. NOTE: This has been disputed by the developer as not a vulnerability since hostname validation does not have anything to do with this issue (i.e. there is no hostname to be sent).
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-21029

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2018-6954
CVE STATUS: Patched
CVE SUMMARY: systemd-tmpfiles in systemd through 237 mishandles symlinks present in non-terminal path components, which allows local users to obtain ownership of arbitrary files via vectors involving creation of a directory and a file under that directory, and later replacing that directory with a symlink. This occurs even if the fs.protected_symlinks sysctl is turned on.
CVSS v2 BASE SCORE: 7.2
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-6954

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2019-15718
CVE STATUS: Patched
CVE SUMMARY: In systemd 240, bus_open_system_watch_bind_with_description in shared/bus-util.c (as used by systemd-resolved to connect to the system D-Bus instance), calls sd_bus_set_trusted, which disables access controls for incoming D-Bus messages. An unprivileged user can exploit this by executing D-Bus methods that should be restricted to privileged users, in order to change the system's DNS resolver settings.
CVSS v2 BASE SCORE: 3.6
CVSS v3 BASE SCORE: 4.4
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-15718

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2019-20386
CVE STATUS: Patched
CVE SUMMARY: An issue was discovered in button_open in login/logind-button.c in systemd before 243. When executing the udevadm trigger command, a memory leak may occur.
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 2.4
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-20386

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2019-3842
CVE STATUS: Patched
CVE SUMMARY: In systemd before v242-rc4, it was discovered that pam_systemd does not properly sanitize the environment before using the XDG_SEAT variable. It is possible for an attacker, in some particular configurations, to set a XDG_SEAT environment variable which allows for commands to be checked against polkit policies using the "allow_active" element rather than "allow_any".
CVSS v2 BASE SCORE: 4.4
CVSS v3 BASE SCORE: 7.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-3842

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2019-3843
CVE STATUS: Patched
CVE SUMMARY: It was discovered that a systemd service that uses DynamicUser property can create a SUID/SGID binary that would be allowed to run as the transient service UID/GID even after the service is terminated. A local attacker may use this flaw to access resources that will be owned by a potentially different service in the future, when the UID/GID will be recycled.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-3843

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2019-3844
CVE STATUS: Patched
CVE SUMMARY: It was discovered that a systemd service that uses DynamicUser property can get new privileges through the execution of SUID binaries, which would allow to create binaries owned by the service transient group with the setgid bit set. A local attacker may use this flaw to access resources that will be owned by a potentially different service in the future, when the GID will be recycled.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-3844

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2019-6454
CVE STATUS: Patched
CVE SUMMARY: An issue was discovered in sd-bus in systemd 239. bus_process_object() in libsystemd/sd-bus/bus-objects.c allocates a variable-length stack buffer for temporarily storing the object path of incoming D-Bus messages. An unprivileged local user can exploit this by sending a specially crafted message to PID1, causing the stack pointer to jump over the stack guard pages into an unmapped memory region and trigger a denial of service (systemd PID1 crash and kernel panic).
CVSS v2 BASE SCORE: 4.9
CVSS v3 BASE SCORE: 5.5
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-6454

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2020-13529
CVE STATUS: Patched
CVE SUMMARY: An exploitable denial-of-service vulnerability exists in Systemd 245. A specially crafted DHCP FORCERENEW packet can cause a server running the DHCP client to be vulnerable to a DHCP ACK spoofing attack. An attacker can forge a pair of FORCERENEW and DCHP ACK packets to reconfigure the server.
CVSS v2 BASE SCORE: 2.9
CVSS v3 BASE SCORE: 6.1
VECTOR: ADJACENT_NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2020-13529

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2020-13776
CVE STATUS: Patched
CVE SUMMARY: systemd through v245 mishandles numerical usernames such as ones composed of decimal digits or 0x followed by hex digits, as demonstrated by use of root privileges when privileges of the 0x0 user account were intended. NOTE: this issue exists because of an incomplete fix for CVE-2017-1000082.
CVSS v2 BASE SCORE: 6.2
CVSS v3 BASE SCORE: 6.7
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2020-13776

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2020-1712
CVE STATUS: Patched
CVE SUMMARY: A heap use-after-free vulnerability was found in systemd before version v245-rc1, where asynchronous Polkit queries are performed while handling dbus messages. A local unprivileged attacker can abuse this flaw to crash systemd services or potentially execute code and elevate their privileges, by sending specially crafted dbus messages.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2020-1712

LAYER: meta
PACKAGE NAME: systemd
PACKAGE VERSION: 1_250.5
CVE: CVE-2021-33910
CVE STATUS: Patched
CVE SUMMARY: basic/unit-name.c in systemd prior to 246.15, 247.8, 248.5, and 249.1 has a Memory Allocation with an Excessive Size Value (involving strdupa and alloca for a pathname controlled by a local attacker) that results in an operating system crash.
CVSS v2 BASE SCORE: 4.9
CVSS v3 BASE SCORE: 5.5
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2021-33910

