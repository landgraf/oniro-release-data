LAYER: meta
PACKAGE NAME: musl
PACKAGE VERSION: 1.2.3+gitAUTOINC+7a43f6fea9
CVE: CVE-2012-2114
CVE STATUS: Patched
CVE SUMMARY: Stack-based buffer overflow in fprintf in musl before 0.8.8 and earlier allows context-dependent attackers to cause a denial of service (crash) or possibly execute arbitrary code via a long string to an unbuffered stream such as stderr.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2012-2114

LAYER: meta
PACKAGE NAME: musl
PACKAGE VERSION: 1.2.3+gitAUTOINC+7a43f6fea9
CVE: CVE-2014-3484
CVE STATUS: Patched
CVE SUMMARY: Multiple stack-based buffer overflows in the __dn_expand function in network/dn_expand.c in musl libc 1.1x before 1.1.2 and 0.9.13 through 1.0.3 allow remote attackers to (1) have unspecified impact via an invalid name length in a DNS response or (2) cause a denial of service (crash) via an invalid name length in a DNS response, related to an infinite loop with no output.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2014-3484

LAYER: meta
PACKAGE NAME: musl
PACKAGE VERSION: 1.2.3+gitAUTOINC+7a43f6fea9
CVE: CVE-2015-1817
CVE STATUS: Patched
CVE SUMMARY: Stack-based buffer overflow in the inet_pton function in network/inet_pton.c in musl libc 0.9.15 through 1.0.4, and 1.1.0 through 1.1.7 allows attackers to have unspecified impact via unknown vectors.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-1817

LAYER: meta
PACKAGE NAME: musl
PACKAGE VERSION: 1.2.3+gitAUTOINC+7a43f6fea9
CVE: CVE-2016-8859
CVE STATUS: Patched
CVE SUMMARY: Multiple integer overflows in the TRE library and musl libc allow attackers to cause memory corruption via a large number of (1) states or (2) tags, which triggers an out-of-bounds write.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-8859

LAYER: meta
PACKAGE NAME: musl
PACKAGE VERSION: 1.2.3+gitAUTOINC+7a43f6fea9
CVE: CVE-2017-15650
CVE STATUS: Patched
CVE SUMMARY: musl libc before 1.1.17 has a buffer overflow via crafted DNS replies because dns_parse_callback in network/lookup_name.c does not restrict the number of addresses, and thus an attacker can provide an unexpected number by sending A records in a reply to an AAAA query.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-15650

LAYER: meta
PACKAGE NAME: musl
PACKAGE VERSION: 1.2.3+gitAUTOINC+7a43f6fea9
CVE: CVE-2019-14697
CVE STATUS: Patched
CVE SUMMARY: musl libc through 1.1.23 has an x87 floating-point stack adjustment imbalance, related to the math/i386/ directory. In some cases, use of this library could introduce out-of-bounds writes that are not present in an application's source code.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-14697

LAYER: meta
PACKAGE NAME: musl
PACKAGE VERSION: 1.2.3+gitAUTOINC+7a43f6fea9
CVE: CVE-2020-28928
CVE STATUS: Patched
CVE SUMMARY: In musl libc through 1.2.1, wcsnrtombs mishandles particular combinations of destination buffer size and source character limit, as demonstrated by an invalid write access (buffer overflow).
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 5.5
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2020-28928

