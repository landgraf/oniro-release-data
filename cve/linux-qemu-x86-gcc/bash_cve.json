{
  "version": "1",
  "package": [
    {
      "name": "bash",
      "layer": "meta",
      "version": "5.1.16",
      "products": [
        {
          "product": "bash",
          "cvesInRecord": "Yes"
        }
      ],
      "issue": [
        {
          "id": "CVE-1999-0491",
          "summary": "The prompt parsing in bash allows a local user to execute commands as another user by creating a directory with the name of the command to execute.",
          "scorev2": "4.6",
          "scorev3": "0.0",
          "vector": "LOCAL",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-1999-0491"
        },
        {
          "id": "CVE-1999-1383",
          "summary": "(1) bash before 1.14.7, and (2) tcsh 6.05 allow local users to gain privileges via directory names that contain shell metacharacters (` back-tick), which can cause the commands enclosed in the directory name to be executed when the shell expands filenames using the \\w option in the PS1 variable.",
          "scorev2": "4.6",
          "scorev3": "0.0",
          "vector": "LOCAL",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-1999-1383"
        },
        {
          "id": "CVE-2010-0002",
          "summary": "The /etc/profile.d/60alias.sh script in the Mandriva bash package for Bash 2.05b, 3.0, 3.2, 3.2.48, and 4.0 enables the --show-control-chars option in LS_OPTIONS, which allows local users to send escape sequences to terminal emulators, or hide the existence of a file, via a crafted filename.",
          "scorev2": "2.1",
          "scorev3": "0.0",
          "vector": "LOCAL",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2010-0002"
        },
        {
          "id": "CVE-2012-3410",
          "summary": "Stack-based buffer overflow in lib/sh/eaccess.c in GNU Bash before 4.2 patch 33 might allow local users to bypass intended restricted shell access via a long filename in /dev/fd, which is not properly handled when expanding the /dev/fd prefix.",
          "scorev2": "4.6",
          "scorev3": "0.0",
          "vector": "LOCAL",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2012-3410"
        },
        {
          "id": "CVE-2012-6711",
          "summary": "A heap-based buffer overflow exists in GNU Bash before 4.3 when wide characters, not supported by the current locale set in the LC_CTYPE environment variable, are printed through the echo built-in function. A local attacker, who can provide data to print through the \"echo -e\" built-in function, may use this flaw to crash a script or execute code with the privileges of the bash process. This occurs because ansicstr() in lib/sh/strtrans.c mishandles u32cconv().",
          "scorev2": "4.6",
          "scorev3": "7.8",
          "vector": "LOCAL",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2012-6711"
        },
        {
          "id": "CVE-2014-6271",
          "summary": "GNU Bash through 4.3 processes trailing strings after function definitions in the values of environment variables, which allows remote attackers to execute arbitrary code via a crafted environment, as demonstrated by vectors involving the ForceCommand feature in OpenSSH sshd, the mod_cgi and mod_cgid modules in the Apache HTTP Server, scripts executed by unspecified DHCP clients, and other situations in which setting the environment occurs across a privilege boundary from Bash execution, aka \"ShellShock.\"  NOTE: the original fix for this issue was incorrect; CVE-2014-7169 has been assigned to cover the vulnerability that is still present after the incorrect fix.",
          "scorev2": "10.0",
          "scorev3": "9.8",
          "vector": "NETWORK",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2014-6271"
        },
        {
          "id": "CVE-2014-6277",
          "summary": "GNU Bash through 4.3 bash43-026 does not properly parse function definitions in the values of environment variables, which allows remote attackers to execute arbitrary code or cause a denial of service (uninitialized memory access, and untrusted-pointer read and write operations) via a crafted environment, as demonstrated by vectors involving the ForceCommand feature in OpenSSH sshd, the mod_cgi and mod_cgid modules in the Apache HTTP Server, scripts executed by unspecified DHCP clients, and other situations in which setting the environment occurs across a privilege boundary from Bash execution.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-6271 and CVE-2014-7169.",
          "scorev2": "10.0",
          "scorev3": "0.0",
          "vector": "NETWORK",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2014-6277"
        },
        {
          "id": "CVE-2014-6278",
          "summary": "GNU Bash through 4.3 bash43-026 does not properly parse function definitions in the values of environment variables, which allows remote attackers to execute arbitrary commands via a crafted environment, as demonstrated by vectors involving the ForceCommand feature in OpenSSH sshd, the mod_cgi and mod_cgid modules in the Apache HTTP Server, scripts executed by unspecified DHCP clients, and other situations in which setting the environment occurs across a privilege boundary from Bash execution.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-6271, CVE-2014-7169, and CVE-2014-6277.",
          "scorev2": "10.0",
          "scorev3": "0.0",
          "vector": "NETWORK",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2014-6278"
        },
        {
          "id": "CVE-2014-7169",
          "summary": "GNU Bash through 4.3 bash43-025 processes trailing strings after certain malformed function definitions in the values of environment variables, which allows remote attackers to write to files or possibly have unknown other impact via a crafted environment, as demonstrated by vectors involving the ForceCommand feature in OpenSSH sshd, the mod_cgi and mod_cgid modules in the Apache HTTP Server, scripts executed by unspecified DHCP clients, and other situations in which setting the environment occurs across a privilege boundary from Bash execution.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-6271.",
          "scorev2": "10.0",
          "scorev3": "0.0",
          "vector": "NETWORK",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2014-7169"
        },
        {
          "id": "CVE-2014-7186",
          "summary": "The redirection implementation in parse.y in GNU Bash through 4.3 bash43-026 allows remote attackers to cause a denial of service (out-of-bounds array access and application crash) or possibly have unspecified other impact via crafted use of here documents, aka the \"redir_stack\" issue.",
          "scorev2": "10.0",
          "scorev3": "0.0",
          "vector": "NETWORK",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2014-7186"
        },
        {
          "id": "CVE-2014-7187",
          "summary": "Off-by-one error in the read_token_word function in parse.y in GNU Bash through 4.3 bash43-026 allows remote attackers to cause a denial of service (out-of-bounds array access and application crash) or possibly have unspecified other impact via deeply nested for loops, aka the \"word_lineno\" issue.",
          "scorev2": "10.0",
          "scorev3": "0.0",
          "vector": "NETWORK",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2014-7187"
        },
        {
          "id": "CVE-2016-0634",
          "summary": "The expansion of '\\h' in the prompt string in bash 4.3 allows remote authenticated users to execute arbitrary code via shell metacharacters placed in 'hostname' of a machine.",
          "scorev2": "6.0",
          "scorev3": "7.5",
          "vector": "NETWORK",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2016-0634"
        },
        {
          "id": "CVE-2016-7543",
          "summary": "Bash before 4.4 allows local users to execute arbitrary commands with root privileges via crafted SHELLOPTS and PS4 environment variables.",
          "scorev2": "7.2",
          "scorev3": "8.4",
          "vector": "LOCAL",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2016-7543"
        },
        {
          "id": "CVE-2016-9401",
          "summary": "popd in bash might allow local users to bypass the restricted shell and cause a use-after-free via a crafted address.",
          "scorev2": "2.1",
          "scorev3": "5.5",
          "vector": "LOCAL",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2016-9401"
        },
        {
          "id": "CVE-2017-5932",
          "summary": "The path autocompletion feature in Bash 4.4 allows local users to gain privileges via a crafted filename starting with a \" (double quote) character and a command substitution metacharacter.",
          "scorev2": "4.6",
          "scorev3": "7.8",
          "vector": "LOCAL",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2017-5932"
        },
        {
          "id": "CVE-2019-18276",
          "summary": "An issue was discovered in disable_priv_mode in shell.c in GNU Bash through 5.0 patch 11. By default, if Bash is run with its effective UID not equal to its real UID, it will drop privileges by setting its effective UID to its real UID. However, it does so incorrectly. On Linux and other systems that support \"saved UID\" functionality, the saved UID is not dropped. An attacker with command execution in the shell can use \"enable -f\" for runtime loading of a new builtin, which can be a shared object that calls setuid() and therefore regains privileges. However, binaries running with an effective UID of 0 are unaffected.",
          "scorev2": "7.2",
          "scorev3": "7.8",
          "vector": "LOCAL",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2019-18276"
        },
        {
          "id": "CVE-2019-9924",
          "summary": "rbash in Bash before 4.4-beta2 did not prevent the shell user from modifying BASH_CMDS, thus allowing the user to execute any command with the permissions of the shell.",
          "scorev2": "7.2",
          "scorev3": "7.8",
          "vector": "LOCAL",
          "status": "Patched",
          "link": "https://nvd.nist.gov/vuln/detail/CVE-2019-9924"
        }
      ]
    }
  ]
}