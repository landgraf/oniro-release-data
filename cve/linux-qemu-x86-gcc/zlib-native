LAYER: meta
PACKAGE NAME: zlib-native
PACKAGE VERSION: 1.2.11
CVE: CVE-2002-0059
CVE STATUS: Patched
CVE SUMMARY: The decompression algorithm in zlib 1.1.3 and earlier, as used in many different utilities and packages, causes inflateEnd to release certain memory more than once (a "double free"), which may allow local and remote attackers to execute arbitrary code via a block of malformed compression data.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2002-0059

LAYER: meta
PACKAGE NAME: zlib-native
PACKAGE VERSION: 1.2.11
CVE: CVE-2003-0107
CVE STATUS: Patched
CVE SUMMARY: Buffer overflow in the gzprintf function in zlib 1.1.4, when zlib is compiled without vsnprintf or when long inputs are truncated using vsnprintf, allows attackers to cause a denial of service or possibly execute arbitrary code.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2003-0107

LAYER: meta
PACKAGE NAME: zlib-native
PACKAGE VERSION: 1.2.11
CVE: CVE-2004-0797
CVE STATUS: Patched
CVE SUMMARY: The error handling in the (1) inflate and (2) inflateBack functions in ZLib compression library 1.2.x allows local users to cause a denial of service (application crash).
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2004-0797

LAYER: meta
PACKAGE NAME: zlib-native
PACKAGE VERSION: 1.2.11
CVE: CVE-2005-1849
CVE STATUS: Patched
CVE SUMMARY: inftrees.h in zlib 1.2.2 allows remote attackers to cause a denial of service (application crash) via an invalid file that causes a large dynamic tree to be produced.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2005-1849

LAYER: meta
PACKAGE NAME: zlib-native
PACKAGE VERSION: 1.2.11
CVE: CVE-2005-2096
CVE STATUS: Patched
CVE SUMMARY: zlib 1.2 and later versions allows remote attackers to cause a denial of service (crash) via a crafted compressed stream with an incomplete code description of a length greater than 1, which leads to a buffer overflow, as demonstrated using a crafted PNG file.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2005-2096

LAYER: meta
PACKAGE NAME: zlib-native
PACKAGE VERSION: 1.2.11
CVE: CVE-2016-9840
CVE STATUS: Patched
CVE SUMMARY: inftrees.c in zlib 1.2.8 might allow context-dependent attackers to have unspecified impact by leveraging improper pointer arithmetic.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 8.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-9840

LAYER: meta
PACKAGE NAME: zlib-native
PACKAGE VERSION: 1.2.11
CVE: CVE-2016-9841
CVE STATUS: Patched
CVE SUMMARY: inffast.c in zlib 1.2.8 might allow context-dependent attackers to have unspecified impact by leveraging improper pointer arithmetic.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-9841

LAYER: meta
PACKAGE NAME: zlib-native
PACKAGE VERSION: 1.2.11
CVE: CVE-2016-9842
CVE STATUS: Patched
CVE SUMMARY: The inflateMark function in inflate.c in zlib 1.2.8 might allow context-dependent attackers to have unspecified impact via vectors involving left shifts of negative integers.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 8.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-9842

LAYER: meta
PACKAGE NAME: zlib-native
PACKAGE VERSION: 1.2.11
CVE: CVE-2016-9843
CVE STATUS: Patched
CVE SUMMARY: The crc32_big function in crc32.c in zlib 1.2.8 might allow context-dependent attackers to have unspecified impact via vectors involving big-endian CRC calculation.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-9843

LAYER: meta
PACKAGE NAME: zlib-native
PACKAGE VERSION: 1.2.11
CVE: CVE-2018-25032
CVE STATUS: Patched
CVE SUMMARY: zlib before 1.2.12 allows memory corruption when deflating (i.e., when compressing) if the input has many distant matches.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-25032

