LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2011-2485
CVE STATUS: Patched
CVE SUMMARY: The gdk_pixbuf__gif_image_load function in gdk-pixbuf/io-gif.c in gdk-pixbuf before 2.23.5 does not properly handle certain return values, which allows remote attackers to cause a denial of service (memory consumption) via a crafted GIF image file.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2011-2485

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2011-2897
CVE STATUS: Patched
CVE SUMMARY: gdk-pixbuf through 2.31.1 has GIF loader buffer overflow when initializing decompression tables due to an input validation flaw
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2011-2897

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2012-2370
CVE STATUS: Patched
CVE SUMMARY: Multiple integer overflows in the read_bitmap_file_data function in io-xbm.c in gdk-pixbuf before 2.26.1 allow remote attackers to cause a denial of service (application crash) via a negative (1) height or (2) width in an XBM file, which triggers a heap-based buffer overflow.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2012-2370

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2015-4491
CVE STATUS: Patched
CVE SUMMARY: Integer overflow in the make_filter_table function in pixops/pixops.c in gdk-pixbuf before 2.31.5, as used in Mozilla Firefox before 40.0 and Firefox ESR 38.x before 38.2 on Linux, Google Chrome on Linux, and other products, allows remote attackers to execute arbitrary code or cause a denial of service (heap-based buffer overflow and application crash) via crafted bitmap dimensions that are mishandled during scaling.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-4491

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2015-7673
CVE STATUS: Patched
CVE SUMMARY: io-tga.c in gdk-pixbuf before 2.32.0 uses heap memory after its allocation failed, which allows remote attackers to cause a denial of service (heap-based buffer overflow and application crash) and possibly execute arbitrary code via a crafted Truevision TGA (TARGA) file.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-7673

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2015-7674
CVE STATUS: Patched
CVE SUMMARY: Integer overflow in the pixops_scale_nearest function in pixops/pixops.c in gdk-pixbuf before 2.32.1 allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via a crafted GIF image file, which triggers a heap-based buffer overflow.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-7674

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2015-8875
CVE STATUS: Patched
CVE SUMMARY: Multiple integer overflows in the (1) pixops_composite_nearest, (2) pixops_composite_color_nearest, and (3) pixops_process functions in pixops/pixops.c in gdk-pixbuf before 2.33.1 allow remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a crafted image, which triggers a heap-based buffer overflow.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 7.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-8875

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2016-6352
CVE STATUS: Patched
CVE SUMMARY: The OneLine32 function in io-ico.c in gdk-pixbuf before 2.35.3 allows remote attackers to cause a denial of service (out-of-bounds write and crash) via crafted dimensions in an ICO file.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-6352

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2017-1000422
CVE STATUS: Patched
CVE SUMMARY: Gnome gdk-pixbuf 2.36.8 and older is vulnerable to several integer overflow in the gif_get_lzw function resulting in memory corruption and potential code execution
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 8.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-1000422

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2017-12447
CVE STATUS: Patched
CVE SUMMARY: GdkPixBuf (aka gdk-pixbuf), possibly 2.32.2, as used by GNOME Nautilus 3.14.3 on Ubuntu 16.04, allows attackers to cause a denial of service (stack corruption) or possibly have unspecified other impact via a crafted file folder.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 7.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-12447

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2017-2862
CVE STATUS: Patched
CVE SUMMARY: An exploitable heap overflow vulnerability exists in the gdk_pixbuf__jpeg_image_load_increment functionality of Gdk-Pixbuf 2.36.6. A specially crafted jpeg file can cause a heap overflow resulting in remote code execution. An attacker can send a file or url to trigger this vulnerability.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 7.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-2862

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2017-2870
CVE STATUS: Patched
CVE SUMMARY: An exploitable integer overflow vulnerability exists in the tiff_image_parse functionality of Gdk-Pixbuf 2.36.6 when compiled with Clang. A specially crafted tiff file can cause a heap-overflow resulting in remote code execution. An attacker can send a file or a URL to trigger this vulnerability.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 7.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-2870

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2017-6311
CVE STATUS: Patched
CVE SUMMARY: gdk-pixbuf-thumbnailer.c in gdk-pixbuf allows context-dependent attackers to cause a denial of service (NULL pointer dereference and application crash) via vectors related to printing an error message.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-6311

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2017-6312
CVE STATUS: Patched
CVE SUMMARY: Integer overflow in io-ico.c in gdk-pixbuf allows context-dependent attackers to cause a denial of service (segmentation fault and application crash) via a crafted image entry offset in an ICO file, which triggers an out-of-bounds read, related to compiler optimizations.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 5.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-6312

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2017-6313
CVE STATUS: Patched
CVE SUMMARY: Integer underflow in the load_resources function in io-icns.c in gdk-pixbuf allows context-dependent attackers to cause a denial of service (out-of-bounds read and program crash) via a crafted image entry size in an ICO file.
CVSS v2 BASE SCORE: 5.8
CVSS v3 BASE SCORE: 7.1
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-6313

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2017-6314
CVE STATUS: Patched
CVE SUMMARY: The make_available_at_least function in io-tiff.c in gdk-pixbuf allows context-dependent attackers to cause a denial of service (infinite loop) via a large TIFF file.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 5.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-6314

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2020-29385
CVE STATUS: Patched
CVE SUMMARY: GNOME gdk-pixbuf (aka GdkPixbuf) before 2.42.2 allows a denial of service (infinite loop) in lzw.c in the function write_indexes. if c->self_code equals 10, self->code_table[10].extends will assign the value 11 to c. The next execution in the loop will assign self->code_table[11].extends to c, which will give the value of 10. This will make the loop run infinitely. This bug can, for example, be triggered by calling this function with a GIF image with LZW compression that is crafted in a special way.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 5.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2020-29385

LAYER: meta
PACKAGE NAME: gdk-pixbuf-native
PACKAGE VERSION: 2.42.6
CVE: CVE-2021-20240
CVE STATUS: Patched
CVE SUMMARY: A flaw was found in gdk-pixbuf in versions before 2.42.0. An integer wraparound leading to an out of bounds write can occur when a crafted GIF image is loaded. An attacker may cause applications to crash or could potentially execute code on the victim system. The highest threat from this vulnerability is to data confidentiality and integrity as well as system availability.
CVSS v2 BASE SCORE: 8.3
CVSS v3 BASE SCORE: 8.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2021-20240

