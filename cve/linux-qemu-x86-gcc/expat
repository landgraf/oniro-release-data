LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2009-3560
CVE STATUS: Patched
CVE SUMMARY: The big2_toUtf8 function in lib/xmltok.c in libexpat in Expat 2.0.1, as used in the XML-Twig module for Perl, allows context-dependent attackers to cause a denial of service (application crash) via an XML document with malformed UTF-8 sequences that trigger a buffer over-read, related to the doProlog function in lib/xmlparse.c, a different vulnerability than CVE-2009-2625 and CVE-2009-3720.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2009-3560

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2009-3720
CVE STATUS: Patched
CVE SUMMARY: The updatePosition function in lib/xmltok_impl.c in libexpat in Expat 2.0.1, as used in Python, PyXML, w3c-libwww, and other software, allows context-dependent attackers to cause a denial of service (application crash) via an XML document with crafted UTF-8 sequences that trigger a buffer over-read, a different vulnerability than CVE-2009-2625.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2009-3720

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2012-0876
CVE STATUS: Patched
CVE SUMMARY: The XML parser (xmlparse.c) in expat before 2.1.0 computes hash values without restricting the ability to trigger hash collisions predictably, which allows context-dependent attackers to cause a denial of service (CPU consumption) via an XML file with many identifiers with the same value.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2012-0876

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2012-1147
CVE STATUS: Patched
CVE SUMMARY: readfilemap.c in expat before 2.1.0 allows context-dependent attackers to cause a denial of service (file descriptor consumption) via a large number of crafted XML files.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2012-1147

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2012-1148
CVE STATUS: Patched
CVE SUMMARY: Memory leak in the poolGrow function in expat/lib/xmlparse.c in expat before 2.1.0 allows context-dependent attackers to cause a denial of service (memory consumption) via a large number of crafted XML files that cause improperly-handled reallocation failures when expanding entities.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2012-1148

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2012-6702
CVE STATUS: Patched
CVE SUMMARY: Expat, when used in a parser that has not called XML_SetHashSalt or passed it a seed of 0, makes it easier for context-dependent attackers to defeat cryptographic protection mechanisms via vectors involving use of the srand function.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 5.9
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2012-6702

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2013-0340
CVE STATUS: Patched
CVE SUMMARY: expat 2.1.0 and earlier does not properly handle entities expansion unless an application developer uses the XML_SetEntityDeclHandler function, which allows remote attackers to cause a denial of service (resource consumption), send HTTP requests to intranet servers, or read arbitrary files via a crafted XML document, aka an XML External Entity (XXE) issue.  NOTE: it could be argued that because expat already provides the ability to disable external entity expansion, the responsibility for resolving this issue lies with application developers; according to this argument, this entry should be REJECTed, and each affected application would need its own CVE.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2013-0340

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2015-1283
CVE STATUS: Patched
CVE SUMMARY: Multiple integer overflows in the XML_GetBuffer function in Expat through 2.1.0, as used in Google Chrome before 44.0.2403.89 and other products, allow remote attackers to cause a denial of service (heap-based buffer overflow) or possibly have unspecified other impact via crafted XML data, a related issue to CVE-2015-2716.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-1283

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2016-0718
CVE STATUS: Patched
CVE SUMMARY: Expat allows context-dependent attackers to cause a denial of service (crash) or possibly execute arbitrary code via a malformed input document, which triggers a buffer overflow.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-0718

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2016-4472
CVE STATUS: Patched
CVE SUMMARY: The overflow protection in Expat is removed by compilers with certain optimization settings, which allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via crafted XML data.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2015-1283 and CVE-2015-2716.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 8.1
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-4472

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2016-5300
CVE STATUS: Patched
CVE SUMMARY: The XML parser in Expat does not use sufficient entropy for hash initialization, which allows context-dependent attackers to cause a denial of service (CPU consumption) via crafted identifiers in an XML document.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2012-0876.
CVSS v2 BASE SCORE: 7.8
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-5300

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2017-11742
CVE STATUS: Patched
CVE SUMMARY: The writeRandomBytes_RtlGenRandom function in xmlparse.c in libexpat in Expat 2.2.1 and 2.2.2 on Windows allows local users to gain privileges via a Trojan horse ADVAPI32.DLL in the current working directory because of an untrusted search path, aka DLL hijacking.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-11742

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2017-9233
CVE STATUS: Patched
CVE SUMMARY: XML External Entity vulnerability in libexpat 2.2.0 and earlier (Expat XML Parser Library) allows attackers to put the parser in an infinite loop using a malformed external entity definition from an external DTD.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-9233

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2018-20843
CVE STATUS: Patched
CVE SUMMARY: In libexpat in Expat before 2.2.7, XML input including XML names that contain a large number of colons could make the XML parser consume a high amount of RAM and CPU resources while processing (enough to be usable for denial-of-service attacks).
CVSS v2 BASE SCORE: 7.8
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-20843

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2019-15903
CVE STATUS: Patched
CVE SUMMARY: In libexpat before 2.2.8, crafted XML input could fool the parser into changing from DTD parsing to document parsing too early; a consecutive call to XML_GetCurrentLineNumber (or XML_GetCurrentColumnNumber) then resulted in a heap-based buffer over-read.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-15903

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2021-45960
CVE STATUS: Patched
CVE SUMMARY: In Expat (aka libexpat) before 2.4.3, a left shift by 29 (or more) places in the storeAtts function in xmlparse.c can lead to realloc misbehavior (e.g., allocating too few bytes, or only freeing memory).
CVSS v2 BASE SCORE: 9.0
CVSS v3 BASE SCORE: 8.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2021-45960

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2021-46143
CVE STATUS: Patched
CVE SUMMARY: In doProlog in xmlparse.c in Expat (aka libexpat) before 2.4.3, an integer overflow exists for m_groupSize.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 7.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2021-46143

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-22822
CVE STATUS: Patched
CVE SUMMARY: addBinding in xmlparse.c in Expat (aka libexpat) before 2.4.3 has an integer overflow.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-22822

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-22823
CVE STATUS: Patched
CVE SUMMARY: build_model in xmlparse.c in Expat (aka libexpat) before 2.4.3 has an integer overflow.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-22823

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-22824
CVE STATUS: Patched
CVE SUMMARY: defineAttribute in xmlparse.c in Expat (aka libexpat) before 2.4.3 has an integer overflow.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-22824

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-22825
CVE STATUS: Patched
CVE SUMMARY: lookup in xmlparse.c in Expat (aka libexpat) before 2.4.3 has an integer overflow.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 8.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-22825

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-22826
CVE STATUS: Patched
CVE SUMMARY: nextScaffoldPart in xmlparse.c in Expat (aka libexpat) before 2.4.3 has an integer overflow.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 8.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-22826

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-22827
CVE STATUS: Patched
CVE SUMMARY: storeAtts in xmlparse.c in Expat (aka libexpat) before 2.4.3 has an integer overflow.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 8.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-22827

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-23852
CVE STATUS: Patched
CVE SUMMARY: Expat (aka libexpat) before 2.4.4 has a signed integer overflow in XML_GetBuffer, for configurations with a nonzero XML_CONTEXT_BYTES.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-23852

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-23990
CVE STATUS: Patched
CVE SUMMARY: Expat (aka libexpat) before 2.4.4 has an integer overflow in the doProlog function.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-23990

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-25235
CVE STATUS: Patched
CVE SUMMARY: xmltok_impl.c in Expat (aka libexpat) before 2.4.5 lacks certain validation of encoding, such as checks for whether a UTF-8 character is valid in a certain context.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-25235

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-25236
CVE STATUS: Patched
CVE SUMMARY: xmlparse.c in Expat (aka libexpat) before 2.4.5 allows attackers to insert namespace-separator characters into namespace URIs.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-25236

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-25313
CVE STATUS: Patched
CVE SUMMARY: In Expat (aka libexpat) before 2.4.5, an attacker can trigger stack exhaustion in build_model via a large nesting depth in the DTD element.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 6.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-25313

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-25314
CVE STATUS: Patched
CVE SUMMARY: In Expat (aka libexpat) before 2.4.5, there is an integer overflow in copyString.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-25314

LAYER: meta
PACKAGE NAME: expat
PACKAGE VERSION: 2.4.7
CVE: CVE-2022-25315
CVE STATUS: Patched
CVE SUMMARY: In Expat (aka libexpat) before 2.4.5, there is an integer overflow in storeRawNames.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-25315

