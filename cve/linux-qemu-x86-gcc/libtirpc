LAYER: meta
PACKAGE NAME: libtirpc
PACKAGE VERSION: 1.3.2
CVE: CVE-2013-1950
CVE STATUS: Patched
CVE SUMMARY: The svc_dg_getargs function in libtirpc 0.2.3 and earlier allows remote attackers to cause a denial of service (rpcbind crash) via a Sun RPC request with crafted arguments that trigger a free of an invalid pointer.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2013-1950

LAYER: meta
PACKAGE NAME: libtirpc
PACKAGE VERSION: 1.3.2
CVE: CVE-2017-8779
CVE STATUS: Patched
CVE SUMMARY: rpcbind through 0.2.4, LIBTIRPC through 1.0.1 and 1.0.2-rc through 1.0.2-rc3, and NTIRPC through 1.4.3 do not consider the maximum RPC data size during memory allocation for XDR strings, which allows remote attackers to cause a denial of service (memory consumption with no subsequent free) via a crafted UDP packet to port 111, aka rpcbomb.
CVSS v2 BASE SCORE: 7.8
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-8779

LAYER: meta
PACKAGE NAME: libtirpc
PACKAGE VERSION: 1.3.2
CVE: CVE-2018-14621
CVE STATUS: Patched
CVE SUMMARY: An infinite loop vulnerability was found in libtirpc before version 1.0.2-rc2. With the port to using poll rather than select, exhaustion of file descriptors would cause the server to enter an infinite loop, consuming a large amount of CPU time and denying service to other clients until restarted.
CVSS v2 BASE SCORE: 7.8
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-14621

LAYER: meta
PACKAGE NAME: libtirpc
PACKAGE VERSION: 1.3.2
CVE: CVE-2018-14622
CVE STATUS: Patched
CVE SUMMARY: A null-pointer dereference vulnerability was found in libtirpc before version 0.3.3-rc3. The return value of makefd_xprt() was not checked in all instances, which could lead to a crash when the server exhausted the maximum number of available file descriptors. A remote attacker could cause an rpc-based application to crash by flooding it with new connections.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-14622

