LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2014-9637
CVE STATUS: Patched
CVE SUMMARY: GNU patch 2.7.2 and earlier allows remote attackers to cause a denial of service (memory consumption and segmentation fault) via a crafted diff file.
CVSS v2 BASE SCORE: 7.1
CVSS v3 BASE SCORE: 5.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2014-9637

LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2015-1196
CVE STATUS: Patched
CVE SUMMARY: GNU patch 2.7.1 allows remote attackers to write to arbitrary files via a symlink attack in a patch file.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-1196

LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2015-1395
CVE STATUS: Patched
CVE SUMMARY: Directory traversal vulnerability in GNU patch versions which support Git-style patching before 2.7.3 allows remote attackers to write to arbitrary files with the permissions of the target user via a .. (dot dot) in a diff file name.
CVSS v2 BASE SCORE: 7.8
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-1395

LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2015-1396
CVE STATUS: Patched
CVE SUMMARY: A Directory Traversal vulnerability exists in the GNU patch before 2.7.4. A remote attacker can write to arbitrary files via a symlink attack in a patch file. NOTE: this issue exists because of an incomplete fix for CVE-2015-1196.
CVSS v2 BASE SCORE: 6.4
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-1396

LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2016-10713
CVE STATUS: Patched
CVE SUMMARY: An issue was discovered in GNU patch before 2.7.6. Out-of-bounds access within pch_write_line() in pch.c can possibly lead to DoS via a crafted input file.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 5.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-10713

LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2018-1000156
CVE STATUS: Patched
CVE SUMMARY: GNU Patch version 2.7.6 contains an input validation vulnerability when processing patch files, specifically the EDITOR_PROGRAM invocation (using ed) can result in code execution. This attack appear to be exploitable via a patch file processed via the patch utility. This is similar to FreeBSD's CVE-2015-1418 however although they share a common ancestry the code bases have diverged over time.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 7.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-1000156

LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2018-20969
CVE STATUS: Patched
CVE SUMMARY: do_ed_script in pch.c in GNU patch through 2.7.6 does not block strings beginning with a ! character. NOTE: this is the same commit as for CVE-2019-13638, but the ! syntax is specific to ed, and is unrelated to a shell metacharacter.
CVSS v2 BASE SCORE: 9.3
CVSS v3 BASE SCORE: 7.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-20969

LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2018-6951
CVE STATUS: Patched
CVE SUMMARY: An issue was discovered in GNU patch through 2.7.6. There is a segmentation fault, associated with a NULL pointer dereference, leading to a denial of service in the intuit_diff_type function in pch.c, aka a "mangled rename" issue.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-6951

LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2018-6952
CVE STATUS: Patched
CVE SUMMARY: A double free exists in the another_hunk function in pch.c in GNU patch through 2.7.6.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-6952

LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2019-13636
CVE STATUS: Patched
CVE SUMMARY: In GNU patch through 2.7.6, the following of symlinks is mishandled in certain cases other than input files. This affects inp.c and util.c.
CVSS v2 BASE SCORE: 5.8
CVSS v3 BASE SCORE: 5.9
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-13636

LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2019-13638
CVE STATUS: Patched
CVE SUMMARY: GNU patch through 2.7.6 is vulnerable to OS shell command injection that can be exploited by opening a crafted patch file that contains an ed style diff payload with shell metacharacters. The ed editor does not need to be present on the vulnerable system. This is different from CVE-2018-1000156.
CVSS v2 BASE SCORE: 9.3
CVSS v3 BASE SCORE: 7.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-13638

LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2019-20633
CVE STATUS: Patched
CVE SUMMARY: GNU patch through 2.7.6 contains a free(p_line[p_end]) Double Free vulnerability in the function another_hunk in pch.c that can cause a denial of service via a crafted patch file. NOTE: this issue exists because of an incomplete fix for CVE-2018-6952.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 5.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-20633

LAYER: meta
PACKAGE NAME: patch-native
PACKAGE VERSION: 2.7.6
CVE: CVE-2021-45261
CVE STATUS: Patched
CVE SUMMARY: An Invalid Pointer vulnerability exists in GNU patch 2.7 via the another_hunk function, which causes a Denial of Service.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 5.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2021-45261

