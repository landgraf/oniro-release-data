LAYER: meta
PACKAGE NAME: groff-native
PACKAGE VERSION: 1.22.4
CVE: CVE-2000-0803
CVE STATUS: Patched
CVE SUMMARY: GNU Groff uses the current working directory to find a device description file, which allows a local user to gain additional privileges by including a malicious postpro directive in the description file, which is executed when another user runs groff.
CVSS v2 BASE SCORE: 10.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2000-0803

LAYER: meta
PACKAGE NAME: groff-native
PACKAGE VERSION: 1.22.4
CVE: CVE-2001-1022
CVE STATUS: Patched
CVE SUMMARY: Format string vulnerability in pic utility in groff 1.16.1 and other versions, and jgroff before 1.15, allows remote attackers to bypass the -S option and execute arbitrary commands via format string specifiers in the plot command.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2001-1022

LAYER: meta
PACKAGE NAME: groff-native
PACKAGE VERSION: 1.22.4
CVE: CVE-2002-0003
CVE STATUS: Patched
CVE SUMMARY: Buffer overflow in the preprocessor in groff 1.16 and earlier allows remote attackers to gain privileges via lpd in the LPRng printing system.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2002-0003

LAYER: meta
PACKAGE NAME: groff-native
PACKAGE VERSION: 1.22.4
CVE: CVE-2004-0969
CVE STATUS: Patched
CVE SUMMARY: The groffer script in the Groff package 1.18 and later versions, as used in Trustix Secure Linux 1.5 through 2.1, and possibly other operating systems, allows local users to overwrite files via a symlink attack on temporary files.
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2004-0969

LAYER: meta
PACKAGE NAME: groff-native
PACKAGE VERSION: 1.22.4
CVE: CVE-2009-5044
CVE STATUS: Patched
CVE SUMMARY: contrib/pdfmark/pdfroff.sh in GNU troff (aka groff) before 1.21 allows local users to overwrite arbitrary files via a symlink attack on a pdf#####.tmp temporary file.
CVSS v2 BASE SCORE: 3.3
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2009-5044

LAYER: meta
PACKAGE NAME: groff-native
PACKAGE VERSION: 1.22.4
CVE: CVE-2009-5078
CVE STATUS: Patched
CVE SUMMARY: contrib/pdfmark/pdfroff.sh in GNU troff (aka groff) before 1.21 launches the Ghostscript program without the -dSAFER option, which allows remote attackers to create, overwrite, rename, or delete arbitrary files via a crafted document.
CVSS v2 BASE SCORE: 6.4
CVSS v3 BASE SCORE: 6.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2009-5078

LAYER: meta
PACKAGE NAME: groff-native
PACKAGE VERSION: 1.22.4
CVE: CVE-2009-5079
CVE STATUS: Patched
CVE SUMMARY: The (1) gendef.sh, (2) doc/fixinfo.sh, and (3) contrib/gdiffmk/tests/runtests.in scripts in GNU troff (aka groff) 1.21 and earlier allow local users to overwrite arbitrary files via a symlink attack on a gro#####.tmp or /tmp/##### temporary file.
CVSS v2 BASE SCORE: 3.3
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2009-5079

LAYER: meta
PACKAGE NAME: groff-native
PACKAGE VERSION: 1.22.4
CVE: CVE-2009-5080
CVE STATUS: Patched
CVE SUMMARY: The (1) contrib/eqn2graph/eqn2graph.sh, (2) contrib/grap2graph/grap2graph.sh, and (3) contrib/pic2graph/pic2graph.sh scripts in GNU troff (aka groff) 1.21 and earlier do not properly handle certain failed attempts to create temporary directories, which might allow local users to overwrite arbitrary files via a symlink attack on a file in a temporary directory, a different vulnerability than CVE-2004-1296.
CVSS v2 BASE SCORE: 3.3
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2009-5080

LAYER: meta
PACKAGE NAME: groff-native
PACKAGE VERSION: 1.22.4
CVE: CVE-2009-5081
CVE STATUS: Patched
CVE SUMMARY: The (1) config.guess, (2) contrib/groffer/perl/groffer.pl, and (3) contrib/groffer/perl/roff2.pl scripts in GNU troff (aka groff) 1.21 and earlier use an insufficient number of X characters in the template argument to the tempfile function, which makes it easier for local users to overwrite arbitrary files via a symlink attack on a temporary file, a different vulnerability than CVE-2004-0969.
CVSS v2 BASE SCORE: 3.3
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2009-5081

LAYER: meta
PACKAGE NAME: groff-native
PACKAGE VERSION: 1.22.4
CVE: CVE-2009-5082
CVE STATUS: Patched
CVE SUMMARY: The (1) configure and (2) config.guess scripts in GNU troff (aka groff) 1.20.1 on Openwall GNU/*/Linux (aka Owl) improperly create temporary files upon a failure of the mktemp function, which makes it easier for local users to overwrite arbitrary files via a symlink attack on a temporary file.
CVSS v2 BASE SCORE: 3.3
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2009-5082

