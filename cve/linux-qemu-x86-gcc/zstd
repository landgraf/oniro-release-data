LAYER: meta
PACKAGE NAME: zstd
PACKAGE VERSION: 1.5.2
CVE: CVE-2019-11922
CVE STATUS: Patched
CVE SUMMARY: A race condition in the one-pass compression functions of Zstandard prior to version 1.3.8 could allow an attacker to write bytes out of bounds if an output buffer smaller than the recommended size was used.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 8.1
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-11922

LAYER: meta
PACKAGE NAME: zstd
PACKAGE VERSION: 1.5.2
CVE: CVE-2021-24031
CVE STATUS: Patched
CVE SUMMARY: In the Zstandard command-line utility prior to v1.4.1, output files were created with default permissions. Correct file permissions (matching the input) would only be set at completion time. Output files could therefore be readable or writable to unintended parties.
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 5.5
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2021-24031

LAYER: meta
PACKAGE NAME: zstd
PACKAGE VERSION: 1.5.2
CVE: CVE-2021-24032
CVE STATUS: Patched
CVE SUMMARY: Beginning in v1.4.1 and prior to v1.4.9, due to an incomplete fix for CVE-2021-24031, the Zstandard command-line utility created output files with default permissions and restricted those permissions immediately afterwards. Output files could therefore momentarily be readable or writable to unintended parties.
CVSS v2 BASE SCORE: 1.9
CVSS v3 BASE SCORE: 4.7
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2021-24032

