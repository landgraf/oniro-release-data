LAYER: meta
PACKAGE NAME: bzip2
PACKAGE VERSION: 1.0.8
CVE: CVE-2002-0759
CVE STATUS: Patched
CVE SUMMARY: bzip2 before 1.0.2 in FreeBSD 4.5 and earlier, OpenLinux 3.1 and 3.1.1, and possibly other operating systems, does not use the O_EXCL flag to create files during decompression and does not warn the user if an existing file would be overwritten, which could allow attackers to overwrite files via a bzip2 archive.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2002-0759

LAYER: meta
PACKAGE NAME: bzip2
PACKAGE VERSION: 1.0.8
CVE: CVE-2002-0760
CVE STATUS: Patched
CVE SUMMARY: Race condition in bzip2 before 1.0.2 in FreeBSD 4.5 and earlier, OpenLinux 3.1 and 3.1.1, and possibly other operating systems, decompresses files with world-readable permissions before setting the permissions to what is specified in the bzip2 archive, which could allow local users to read the files as they are being decompressed.
CVSS v2 BASE SCORE: 1.2
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2002-0760

LAYER: meta
PACKAGE NAME: bzip2
PACKAGE VERSION: 1.0.8
CVE: CVE-2002-0761
CVE STATUS: Patched
CVE SUMMARY: bzip2 before 1.0.2 in FreeBSD 4.5 and earlier, OpenLinux 3.1 and 3.1.1, and possibly systems, uses the permissions of symbolic links instead of the actual files when creating an archive, which could cause the files to be extracted with less restrictive permissions than intended.
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2002-0761

LAYER: meta
PACKAGE NAME: bzip2
PACKAGE VERSION: 1.0.8
CVE: CVE-2005-0953
CVE STATUS: Patched
CVE SUMMARY: Race condition in bzip2 1.0.2 and earlier allows local users to modify permissions of arbitrary files via a hard link attack on a file while it is being decompressed, whose permissions are changed by bzip2 after the decompression is complete.
CVSS v2 BASE SCORE: 3.7
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2005-0953

LAYER: meta
PACKAGE NAME: bzip2
PACKAGE VERSION: 1.0.8
CVE: CVE-2005-1260
CVE STATUS: Patched
CVE SUMMARY: bzip2 allows remote attackers to cause a denial of service (hard drive consumption) via a crafted bzip2 file that causes an infinite loop (a.k.a "decompression bomb").
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2005-1260

LAYER: meta
PACKAGE NAME: bzip2
PACKAGE VERSION: 1.0.8
CVE: CVE-2008-1372
CVE STATUS: Patched
CVE SUMMARY: bzlib.c in bzip2 before 1.0.5 allows user-assisted remote attackers to cause a denial of service (crash) via a crafted file that triggers a buffer over-read, as demonstrated by the PROTOS GENOME test suite for Archive Formats.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2008-1372

LAYER: meta
PACKAGE NAME: bzip2
PACKAGE VERSION: 1.0.8
CVE: CVE-2010-0405
CVE STATUS: Patched
CVE SUMMARY: Integer overflow in the BZ2_decompress function in decompress.c in bzip2 and libbzip2 before 1.0.6 allows context-dependent attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a crafted compressed file.
CVSS v2 BASE SCORE: 5.1
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2010-0405

LAYER: meta
PACKAGE NAME: bzip2
PACKAGE VERSION: 1.0.8
CVE: CVE-2011-4089
CVE STATUS: Patched
CVE SUMMARY: The bzexe command in bzip2 1.0.5 and earlier generates compressed executables that do not properly handle temporary files during extraction, which allows local users to execute arbitrary code by precreating a temporary directory.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2011-4089

LAYER: meta
PACKAGE NAME: bzip2
PACKAGE VERSION: 1.0.8
CVE: CVE-2016-3189
CVE STATUS: Patched
CVE SUMMARY: Use-after-free vulnerability in bzip2recover in bzip2 1.0.6 allows remote attackers to cause a denial of service (crash) via a crafted bzip2 file, related to block ends set to before the start of the block.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 6.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-3189

LAYER: meta
PACKAGE NAME: bzip2
PACKAGE VERSION: 1.0.8
CVE: CVE-2019-12900
CVE STATUS: Patched
CVE SUMMARY: BZ2_decompress in decompress.c in bzip2 through 1.0.6 has an out-of-bounds write when there are many selectors.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-12900

