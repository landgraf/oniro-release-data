LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-1999-0034
CVE STATUS: Patched
CVE SUMMARY: Buffer overflow in suidperl (sperl), Perl 4.x and 5.x.
CVSS v2 BASE SCORE: 7.2
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-1999-0034

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-1999-1386
CVE STATUS: Patched
CVE SUMMARY: Perl 5.004_04 and earlier follows symbolic links when running with the -e option, which allows local users to overwrite arbitrary files via a symlink attack on the /tmp/perl-eaXXXXX file.
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-1999-1386

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2000-0703
CVE STATUS: Patched
CVE SUMMARY: suidperl (aka sperl) does not properly cleanse the escape sequence "~!" before calling /bin/mail to send an error report, which allows local users to gain privileges by setting the "interactive" environmental variable and calling suidperl with a filename that contains the escape sequence.
CVSS v2 BASE SCORE: 7.2
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2000-0703

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2003-0900
CVE STATUS: Patched
CVE SUMMARY: Perl 5.8.1 on Fedora Core does not properly initialize the random number generator when forking, which makes it easier for attackers to predict random numbers.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2003-0900

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2004-0377
CVE STATUS: Patched
CVE SUMMARY: Buffer overflow in the win32_stat function for (1) ActiveState's ActivePerl and (2) Larry Wall's Perl before 5.8.3 allows local or remote attackers to execute arbitrary commands via filenames that end in a backslash character.
CVSS v2 BASE SCORE: 10.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2004-0377

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2004-0452
CVE STATUS: Patched
CVE SUMMARY: Race condition in the rmtree function in the File::Path module in Perl 5.6.1 and 5.8.4 sets read/write permissions for the world, which allows local users to delete arbitrary files and directories, and possibly read files and directories, via a symlink attack.
CVSS v2 BASE SCORE: 2.6
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2004-0452

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2004-0976
CVE STATUS: Patched
CVE SUMMARY: Multiple scripts in the perl package in Trustix Secure Linux 1.5 through 2.1 and other operating systems allows local users to overwrite files via a symlink attack on temporary files.
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2004-0976

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2004-2286
CVE STATUS: Patched
CVE SUMMARY: Integer overflow in the duplication operator in ActivePerl allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a large multiplier, which may trigger a buffer overflow.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2004-2286

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2005-0155
CVE STATUS: Patched
CVE SUMMARY: The PerlIO implementation in Perl 5.8.0, when installed with setuid support (sperl), allows local users to create arbitrary files via the PERLIO_DEBUG variable.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2005-0155

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2005-0156
CVE STATUS: Patched
CVE SUMMARY: Buffer overflow in the PerlIO implementation in Perl 5.8.0, when installed with setuid support (sperl), allows local users to execute arbitrary code by setting the PERLIO_DEBUG variable and executing a Perl script whose full pathname contains a long directory tree.
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2005-0156

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2005-0448
CVE STATUS: Patched
CVE SUMMARY: Race condition in the rmtree function in File::Path.pm in Perl before 5.8.4 allows local users to create arbitrary setuid binaries in the tree being deleted, a different vulnerability than CVE-2004-0452.
CVSS v2 BASE SCORE: 1.2
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2005-0448

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2005-3962
CVE STATUS: Patched
CVE SUMMARY: Integer overflow in the format string functionality (Perl_sv_vcatpvfn) in Perl 5.9.2 and 5.8.6 Perl allows attackers to overwrite arbitrary memory and possibly execute arbitrary code via format string specifiers with large values, which causes an integer wrap and leads to a buffer overflow, as demonstrated using format string vulnerabilities in Perl applications.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2005-3962

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2005-4278
CVE STATUS: Patched
CVE SUMMARY: Untrusted search path vulnerability in Perl before 5.8.7-r1 on Gentoo Linux allows local users in the portage group to gain privileges via a malicious shared object in the Portage temporary build directory, which is part of the RUNPATH.
CVSS v2 BASE SCORE: 7.2
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2005-4278

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2007-5116
CVE STATUS: Patched
CVE SUMMARY: Buffer overflow in the polymorphic opcode support in the Regular Expression Engine (regcomp.c) in Perl 5.8 allows context-dependent attackers to execute arbitrary code by switching from byte to Unicode (UTF) characters in a regular expression.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2007-5116

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2008-1927
CVE STATUS: Patched
CVE SUMMARY: Double free vulnerability in Perl 5.8.8 allows context-dependent attackers to cause a denial of service (memory corruption and crash) via a crafted regular expression containing UTF8 characters.  NOTE: this issue might only be present on certain operating systems.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2008-1927

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2008-2827
CVE STATUS: Patched
CVE SUMMARY: The rmtree function in lib/File/Path.pm in Perl 5.10 does not properly check permissions before performing a chmod, which allows local users to modify the permissions of arbitrary files via a symlink attack, a different vulnerability than CVE-2005-0448 and CVE-2004-0452.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2008-2827

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2009-3626
CVE STATUS: Patched
CVE SUMMARY: Perl 5.10.1 allows context-dependent attackers to cause a denial of service (application crash) via a UTF-8 character with a large, invalid codepoint, which is not properly handled during a regular-expression match.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2009-3626

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2010-1158
CVE STATUS: Patched
CVE SUMMARY: Integer overflow in the regular expression engine in Perl 5.8.x allows context-dependent attackers to cause a denial of service (stack consumption and application crash) by matching a crafted regular expression against a long string.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2010-1158

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2010-4777
CVE STATUS: Patched
CVE SUMMARY: The Perl_reg_numbered_buff_fetch function in Perl 5.10.0, 5.12.0, 5.14.0, and other versions, when running with debugging enabled, allows context-dependent attackers to cause a denial of service (assertion failure and application exit) via crafted input that is not properly handled when using certain regular expressions, as demonstrated by causing SpamAssassin and OCSInventory to crash.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2010-4777

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2011-0761
CVE STATUS: Patched
CVE SUMMARY: Perl 5.10.x allows context-dependent attackers to cause a denial of service (NULL pointer dereference and application crash) by leveraging an ability to inject arguments into a (1) getpeername, (2) readdir, (3) closedir, (4) getsockname, (5) rewinddir, (6) tell, or (7) telldir function call.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2011-0761

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2011-1487
CVE STATUS: Patched
CVE SUMMARY: The (1) lc, (2) lcfirst, (3) uc, and (4) ucfirst functions in Perl 5.10.x, 5.11.x, and 5.12.x through 5.12.3, and 5.13.x through 5.13.11, do not apply the taint attribute to the return value upon processing tainted input, which might allow context-dependent attackers to bypass the taint protection mechanism via a crafted string.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2011-1487

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2011-2728
CVE STATUS: Patched
CVE SUMMARY: The bsd_glob function in the File::Glob module for Perl before 5.14.2 allows context-dependent attackers to cause a denial of service (crash) via a glob expression with the GLOB_ALTDIRFUNC flag, which triggers an uninitialized pointer dereference.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2011-2728

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2011-2939
CVE STATUS: Patched
CVE SUMMARY: Off-by-one error in the decode_xs function in Unicode/Unicode.xs in the Encode module before 2.44, as used in Perl before 5.15.6, might allow context-dependent attackers to cause a denial of service (memory corruption) via a crafted Unicode string, which triggers a heap-based buffer overflow.
CVSS v2 BASE SCORE: 5.1
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2011-2939

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2012-1151
CVE STATUS: Patched
CVE SUMMARY: Multiple format string vulnerabilities in dbdimp.c in DBD::Pg (aka DBD-Pg or libdbd-pg-perl) module before 2.19.0 for Perl allow remote PostgreSQL database servers to cause a denial of service (process crash) via format string specifiers in (1) a crafted database warning to the pg_warn function or (2) a crafted DBD statement to the dbd_st_prepare function.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2012-1151

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2012-5195
CVE STATUS: Patched
CVE SUMMARY: Heap-based buffer overflow in the Perl_repeatcpy function in util.c in Perl 5.12.x before 5.12.5, 5.14.x before 5.14.3, and 5.15.x before 15.15.5 allows context-dependent attackers to cause a denial of service (memory consumption and crash) or possibly execute arbitrary code via the 'x' string repeat operator.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2012-5195

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2012-6329
CVE STATUS: Patched
CVE SUMMARY: The _compile function in Maketext.pm in the Locale::Maketext implementation in Perl before 5.17.7 does not properly handle backslashes and fully qualified method names during compilation of bracket notation, which allows context-dependent attackers to execute arbitrary commands via crafted input to an application that accepts translation strings from users, as demonstrated by the TWiki application before 5.1.3, and the Foswiki application 1.0.x through 1.0.10 and 1.1.x through 1.1.6.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2012-6329

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2013-1667
CVE STATUS: Patched
CVE SUMMARY: The rehash mechanism in Perl 5.8.2 through 5.16.x allows context-dependent attackers to cause a denial of service (memory consumption and crash) via a crafted hash key.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2013-1667

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2013-7422
CVE STATUS: Patched
CVE SUMMARY: Integer underflow in regcomp.c in Perl before 5.20, as used in Apple OS X before 10.10.5 and other products, allows context-dependent attackers to execute arbitrary code or cause a denial of service (application crash) via a long digit string associated with an invalid backreference within a regular expression.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2013-7422

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2014-4330
CVE STATUS: Patched
CVE SUMMARY: The Dumper method in Data::Dumper before 2.154, as used in Perl 5.20.1 and earlier, allows context-dependent attackers to cause a denial of service (stack consumption and crash) via an Array-Reference with many nested Array-References, which triggers a large number of recursive calls to the DD_dump function.
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2014-4330

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2015-8608
CVE STATUS: Patched
CVE SUMMARY: The VDir::MapPathA and VDir::MapPathW functions in Perl 5.22 allow remote attackers to cause a denial of service (out-of-bounds read) and possibly execute arbitrary code via a crafted (1) drive letter or (2) pInName argument.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-8608

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2015-8853
CVE STATUS: Patched
CVE SUMMARY: The (1) S_reghop3, (2) S_reghop4, and (3) S_reghopmaybe3 functions in regexec.c in Perl before 5.24.0 allow context-dependent attackers to cause a denial of service (infinite loop) via crafted utf-8 data, as demonstrated by "a\x80."
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-8853

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2016-1238
CVE STATUS: Patched
CVE SUMMARY: (1) cpan/Archive-Tar/bin/ptar, (2) cpan/Archive-Tar/bin/ptardiff, (3) cpan/Archive-Tar/bin/ptargrep, (4) cpan/CPAN/scripts/cpan, (5) cpan/Digest-SHA/shasum, (6) cpan/Encode/bin/enc2xs, (7) cpan/Encode/bin/encguess, (8) cpan/Encode/bin/piconv, (9) cpan/Encode/bin/ucmlint, (10) cpan/Encode/bin/unidump, (11) cpan/ExtUtils-MakeMaker/bin/instmodsh, (12) cpan/IO-Compress/bin/zipdetails, (13) cpan/JSON-PP/bin/json_pp, (14) cpan/Test-Harness/bin/prove, (15) dist/ExtUtils-ParseXS/lib/ExtUtils/xsubpp, (16) dist/Module-CoreList/corelist, (17) ext/Pod-Html/bin/pod2html, (18) utils/c2ph.PL, (19) utils/h2ph.PL, (20) utils/h2xs.PL, (21) utils/libnetcfg.PL, (22) utils/perlbug.PL, (23) utils/perldoc.PL, (24) utils/perlivp.PL, and (25) utils/splain.PL in Perl 5.x before 5.22.3-RC2 and 5.24 before 5.24.1-RC2 do not properly remove . (period) characters from the end of the includes directory array, which might allow local users to gain privileges via a Trojan horse module under the current working directory.
CVSS v2 BASE SCORE: 7.2
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-1238

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2016-2381
CVE STATUS: Patched
CVE SUMMARY: Perl might allow context-dependent attackers to bypass the taint protection mechanism in a child process via duplicate environment variables in envp.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-2381

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2016-6185
CVE STATUS: Patched
CVE SUMMARY: The XSLoader::load method in XSLoader in Perl does not properly locate .so files when called in a string eval, which might allow local users to execute arbitrary code via a Trojan horse library under the current working directory.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2016-6185

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2017-12814
CVE STATUS: Patched
CVE SUMMARY: Stack-based buffer overflow in the CPerlHost::Add method in win32/perlhost.h in Perl before 5.24.3-RC1 and 5.26.x before 5.26.1-RC1 on Windows allows attackers to execute arbitrary code via a long environment variable.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-12814

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2017-12837
CVE STATUS: Patched
CVE SUMMARY: Heap-based buffer overflow in the S_regatom function in regcomp.c in Perl 5 before 5.24.3-RC1 and 5.26.x before 5.26.1-RC1 allows remote attackers to cause a denial of service (out-of-bounds write) via a regular expression with a '\N{}' escape and the case-insensitive modifier.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-12837

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2017-12883
CVE STATUS: Patched
CVE SUMMARY: Buffer overflow in the S_grok_bslash_N function in regcomp.c in Perl 5 before 5.24.3-RC1 and 5.26.x before 5.26.1-RC1 allows remote attackers to disclose sensitive information or cause a denial of service (application crash) via a crafted regular expression with an invalid '\N{U+...}' escape.
CVSS v2 BASE SCORE: 6.4
CVSS v3 BASE SCORE: 9.1
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-12883

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2018-12015
CVE STATUS: Patched
CVE SUMMARY: In Perl through 5.26.2, the Archive::Tar module allows remote attackers to bypass a directory-traversal protection mechanism, and overwrite arbitrary files, via an archive file containing a symlink and a regular file with the same name.
CVSS v2 BASE SCORE: 6.4
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-12015

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2018-18311
CVE STATUS: Patched
CVE SUMMARY: Perl before 5.26.3 and 5.28.x before 5.28.1 has a buffer overflow via a crafted regular expression that triggers invalid write operations.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-18311

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2018-18312
CVE STATUS: Patched
CVE SUMMARY: Perl before 5.26.3 and 5.28.0 before 5.28.1 has a buffer overflow via a crafted regular expression that triggers invalid write operations.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-18312

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2018-18313
CVE STATUS: Patched
CVE SUMMARY: Perl before 5.26.3 has a buffer over-read via a crafted regular expression that triggers disclosure of sensitive information from process memory.
CVSS v2 BASE SCORE: 6.4
CVSS v3 BASE SCORE: 9.1
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-18313

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2018-18314
CVE STATUS: Patched
CVE SUMMARY: Perl before 5.26.3 has a buffer overflow via a crafted regular expression that triggers invalid write operations.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-18314

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2018-6797
CVE STATUS: Patched
CVE SUMMARY: An issue was discovered in Perl 5.18 through 5.26. A crafted regular expression can cause a heap-based buffer overflow, with control over the bytes written.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-6797

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2018-6798
CVE STATUS: Patched
CVE SUMMARY: An issue was discovered in Perl 5.22 through 5.26. Matching a crafted locale dependent regular expression can cause a heap-based buffer over-read and potentially information disclosure.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-6798

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2018-6913
CVE STATUS: Patched
CVE SUMMARY: Heap-based buffer overflow in the pack function in Perl before 5.26.2 allows context-dependent attackers to execute arbitrary code via a large item count.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 9.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-6913

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2020-10543
CVE STATUS: Patched
CVE SUMMARY: Perl before 5.30.3 on 32-bit platforms allows a heap-based buffer overflow because nested regular expression quantifiers have an integer overflow.
CVSS v2 BASE SCORE: 6.4
CVSS v3 BASE SCORE: 8.2
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2020-10543

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2020-10878
CVE STATUS: Patched
CVE SUMMARY: Perl before 5.30.3 has an integer overflow related to mishandling of a "PL_regkind[OP(n)] == NOTHING" situation. A crafted regular expression could lead to malformed bytecode with a possibility of instruction injection.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 8.6
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2020-10878

LAYER: meta
PACKAGE NAME: perl
PACKAGE VERSION: 5.34.1
CVE: CVE-2020-12723
CVE STATUS: Patched
CVE SUMMARY: regcomp.c in Perl before 5.30.3 allows a buffer overflow via a crafted regular expression because of recursive S_study_chunk calls.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2020-12723

