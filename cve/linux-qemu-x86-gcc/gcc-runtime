LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-1999-1439
CVE STATUS: Patched
CVE SUMMARY: gcc 2.7.2 allows local users to overwrite arbitrary files via a symlink attack on temporary .i, .s, or .o files.
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-1999-1439

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2000-1219
CVE STATUS: Patched
CVE SUMMARY: The -ftrapv compiler option in gcc and g++ 3.3.3 and earlier does not handle all types of integer overflows, which may leave applications vulnerable to vulnerabilities related to overflows.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2000-1219

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2002-2439
CVE STATUS: Patched
CVE SUMMARY: Integer overflow in the new[] operator in gcc before 4.8.0 allows attackers to have unspecified impacts.
CVSS v2 BASE SCORE: 4.6
CVSS v3 BASE SCORE: 7.8
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2002-2439

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2006-1902
CVE STATUS: Patched
CVE SUMMARY: fold_binary in fold-const.c in GNU Compiler Collection (gcc) 4.1 improperly handles pointer overflow when folding a certain expr comparison to a corresponding offset comparison in cases other than EQ_EXPR and NE_EXPR, which might introduce buffer overflow vulnerabilities into applications that could be exploited by context-dependent attackers.NOTE: the vendor states that the essence of the issue is "not correctly interpreting an offset to a pointer as a signed value."
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 0.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2006-1902

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2008-1367
CVE STATUS: Patched
CVE SUMMARY: gcc 4.3.x does not generate a cld instruction while compiling functions used for string manipulation such as memcpy and memmove on x86 and i386, which can prevent the direction flag (DF) from being reset in violation of ABI conventions and cause data to be copied in the wrong direction during signal handling in the Linux kernel, which might allow context-dependent attackers to trigger memory corruption. NOTE: this issue was originally reported for CPU consumption in SBCL.
CVSS v2 BASE SCORE: 7.5
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2008-1367

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2008-1685
CVE STATUS: Patched
CVE SUMMARY: ** DISPUTED **  gcc 4.2.0 through 4.3.0 in GNU Compiler Collection, when casts are not used, considers the sum of a pointer and an int to be greater than or equal to the pointer, which might lead to removal of length testing code that was intended as a protection mechanism against integer overflow and buffer overflow attacks, and provide no diagnostic message about this removal. NOTE: the vendor has determined that this compiler behavior is correct according to section 6.5.6 of the C99 standard (aka ISO/IEC 9899:1999).
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2008-1685

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2013-4598
CVE STATUS: Patched
CVE SUMMARY: The Groups, Communities and Co (GCC) module 7.x-1.x before 7.x-1.1 for Drupal does not properly check permission, which allows remote attackers to access the configuration pages via unspecified vectors.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2013-4598

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2015-5276
CVE STATUS: Patched
CVE SUMMARY: The std::random_device class in libstdc++ in the GNU Compiler Collection (aka GCC) before 4.9.4 does not properly handle short reads from blocking sources, which makes it easier for context-dependent attackers to predict the random values via unspecified vectors.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 0.0
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2015-5276

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2017-11671
CVE STATUS: Patched
CVE SUMMARY: Under certain circumstances, the ix86_expand_builtin function in i386.c in GNU Compiler Collection (GCC) version 4.6, 4.7, 4.8, 4.9, 5 before 5.5, and 6 before 6.4 will generate instruction sequences that clobber the status flag of the RDRAND and RDSEED intrinsics before it can be read, potentially causing failures of these instructions to go unreported. This could potentially lead to less randomness in random number generation.
CVSS v2 BASE SCORE: 2.1
CVSS v3 BASE SCORE: 4.0
VECTOR: LOCAL
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2017-11671

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2018-12886
CVE STATUS: Patched
CVE SUMMARY: stack_protect_prologue in cfgexpand.c and stack_protect_epilogue in function.c in GNU Compiler Collection (GCC) 4.1 through 8 (under certain circumstances) generate instruction sequences when targeting ARM targets that spill the address of the stack protector guard, which allows an attacker to bypass the protection of -fstack-protector, -fstack-protector-all, -fstack-protector-strong, and -fstack-protector-explicit against stack overflow by controlling what the stack canary is compared against.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 8.1
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2018-12886

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2019-15847
CVE STATUS: Patched
CVE SUMMARY: The POWER9 backend in GNU Compiler Collection (GCC) before version 10 could optimize multiple calls of the __builtin_darn intrinsic into a single call, thus reducing the entropy of the random number generator. This occurred because a volatile operation was not specified. For example, within a single execution of a program, the output of every __builtin_darn() call may be the same.
CVSS v2 BASE SCORE: 5.0
CVSS v3 BASE SCORE: 7.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2019-15847

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2021-37322
CVE STATUS: Ignored
CVE SUMMARY: GCC c++filt v2.26 was discovered to contain a use-after-free vulnerability via the component cplus-dem.c.
CVSS v2 BASE SCORE: 6.8
CVSS v3 BASE SCORE: 7.8
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2021-37322

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2021-46195
CVE STATUS: Patched
CVE SUMMARY: GCC v12.0 was discovered to contain an uncontrolled recursion via the component libiberty/rust-demangle.c. This vulnerability allows attackers to cause a Denial of Service (DoS) by consuming excessive CPU and memory resources.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 5.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2021-46195

LAYER: meta
PACKAGE NAME: gcc-runtime
PACKAGE VERSION: 11.2.0
CVE: CVE-2022-27943
CVE STATUS: Patched
CVE SUMMARY: libiberty/rust-demangle.c in GNU GCC 11.2 allows stack consumption in demangle_const, as demonstrated by nm-new.
CVSS v2 BASE SCORE: 4.3
CVSS v3 BASE SCORE: 5.5
VECTOR: NETWORK
MORE INFORMATION: https://nvd.nist.gov/vuln/detail/CVE-2022-27943

